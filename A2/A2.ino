// Praktikum Sensortechnik
// Modul 1: Beschleunigungssensor
// Aufgabe 2
// Paul Krüger

#include <Wire.h>
#include <Adafruit_MMA8451.h>
#include <Adafruit_Sensor.h>

Adafruit_MMA8451 mma = Adafruit_MMA8451();
float x_cal = 0;
float y_cal = 0;
float z_cal = 0;

void setup(void) {
  // setup
  Serial.begin(9600);
  Serial.println("Sensortechnik Praktikum");
  if (! mma.begin()) {
    Serial.println("Couldnt start");
    while (1);
  }
  mma.setRange(MMA8451_RANGE_2_G);

  // calibration
  for (int i=0; i<10; i++) {
    mma.read();
    sensors_event_t event; 
    mma.getEvent(&event);
    x_cal += mma.x;
    y_cal += mma.y;
    z_cal += mma.z;
    delay(50);
  }
  x_cal = x_cal / 10;
  y_cal = y_cal / 10;
  z_cal = z_cal / 10;
}

void loop() {
  // read sensor data
  mma.read();  
  Serial.print(mma.x - x_cal);
  Serial.print(",");
  Serial.print(mma.y - y_cal);
  Serial.print(",");
  Serial.print(mma.z - z_cal);
  Serial.println();
  delay(500);
}
