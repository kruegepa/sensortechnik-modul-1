# Praktikum Sensortechnik
# Modul 1: Beschleunigungssensor
# Aufgabe 2
# Paul Krüger

# imports
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats
import math

# import data
path = "/Users/kruegepa/Desktop/Universität/5. Semester/Sensortechnik/Modul 1/Beschleunigungssensoren/A2/A2 Data.txt"
data = pd.read_csv(path)

# plot figure
plt.figure(figsize=(10, 4))

plt.subplot(1,3,1)
bins, counts = np.histogram(data["X"])
plt.stairs(bins, counts, fill=True, label="$\sigma$ = " + str(np.round(np.std(data["X"]), 2)))
mu = np.mean(data["X"])
sigma = np.std(data["X"])
x = np.linspace(mu - 3*sigma, mu + 3*sigma, 100)
plt.plot(x, 470*stats.norm.pdf(x, mu, sigma), label="Normalverteilung", color="black")
plt.legend()
plt.ylim((0, 30))
plt.xlabel("x-Wert")
plt.ylabel("Häufigkeit")
print("--------------------------")
print("std-x: " + str(np.std(data["X"])))
print("mean-x: " + str(np.mean(data["X"])))

plt.subplot(1,3,2)
bins, counts = np.histogram(data["Y"])
plt.stairs(bins, counts, fill=True, label="$\sigma$ = " + str(np.round(np.std(data["Y"]), 2)), color="darkorange")
mu = np.mean(data["Y"])
sigma = np.std(data["Y"])
x = np.linspace(mu - 3*sigma, mu + 3*sigma, 100)
plt.plot(x, 470*stats.norm.pdf(x, mu, sigma), label="Normalverteilung", color="black")
plt.legend(loc="upper right")
plt.ylim((0, 30))
plt.xlabel("y-Wert")
print("std-y: " + str(np.std(data["Y"])))
print("mean-y: " + str(np.mean(data["Y"])))

plt.subplot(1,3,3)
bins, counts = np.histogram(data["Z"])
plt.stairs(bins, counts, fill=True, label="$\sigma$ = " + str(np.round(np.std(data["Z"]), 2)), color="green")
mu = np.mean(data["Z"])
sigma = np.std(data["Z"])
x = np.linspace(mu - 3*sigma, mu + 3*sigma, 100)
plt.plot(x, 470*stats.norm.pdf(x, mu, sigma), label="Normalverteilung", color="black")
plt.legend()
plt.ylim((0, 30))
plt.xlabel("z-Wert")
print("std-z: " + str(np.std(data["Z"])))
print("mean-z: " + str(np.mean(data["Z"])))
print("--------------------------")

plt.savefig("/Users/kruegepa/Desktop/Universität/5. Semester/Sensortechnik/Modul 1/Beschleunigungssensoren/A2/A2 Data.png", dpi=500)
plt.show()
