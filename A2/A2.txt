Praktikum Sensortechnik
Modul 1: Beschleunigungssensor
Aufgabe 2
Paul Krüger

Was ist die maximal mögliche Datenrate?
void loop() {
  unsigned long start = micros();
  mma.read();  
  unsigned long stop = micros();
  Serial.println(stop - start);
  Serial.println();
  delay(1000);
}

-> Zeit pro Messung: 1353 us
