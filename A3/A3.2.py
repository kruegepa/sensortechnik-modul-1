# Praktikum Sensortechnik
# Modul 1: Beschleunigungssensor
# Aufgabe 3.2
# Paul Krüger

# imports
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


# import data
path = "A3.2 Data.txt"
data = pd.read_csv(path, skiprows=1)

# plot figure
plt.figure(figsize=(10, 4))
plt.plot(data["X"], label="x-Achse")
plt.plot(data["Y"], label="y-Achse", color="darkorange")
plt.plot(data["Z"], label="z-Achse", color="green")
plt.xlabel("Zeit in ms")
plt.ylabel("Beschleunigung in m/s^2")
plt.legend()

plt.savefig("./A3.2.png", dpi=500)
plt.show()
