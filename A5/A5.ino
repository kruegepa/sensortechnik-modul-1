#include <D7S.h>

//old earthquake data
float oldSI = 0;
float oldPGA = 0;

//flag variables to handle collapse/shutoff only one time during an earthquake
bool shutoffHandled = false;
bool collapseHandled = false;


void setup()
{
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial)
  {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println("Praktikum Sensortechnik");
  Serial.print("Starting D7S communications (it may take some time)...");
  //start D7S connection
  D7S.begin();
  //wait until the D7S is ready
  while (!D7S.isReady())
  {
    Serial.print(".");
    delay(500);
  }
  Serial.println("STARTED");

  //setting the D7S to switch the axis at inizialization time
  Serial.println("Setting D7S sensor to switch axis at inizialization time.");
  D7S.setAxis(SWITCH_AT_INSTALLATION);

  Serial.println("Initializing the D7S sensor in 2 seconds. Please keep it steady during the initializing process.");
  delay(2000);
  Serial.print("Initializing...");
  //start the initial installation procedure
  D7S.initialize();
  //wait until the D7S is ready (the initializing process is ended)
  while (!D7S.isReady())
  {
    Serial.print(".");
    delay(500);
  }
  Serial.println("INITIALIZED!");


  //reset the events shutoff/collapse memorized into the D7S
  D7S.resetEvents();

  Serial.println("\nListening for earthquakes!");
}


void loop() {
    for (int i=0; i<100; i++){
      Serial.print(D7S.getInstantaneusSI());
      Serial.print(",");
      Serial.print(D7S.getInstantaneusPGA());
      Serial.println();
      delay(100);
  }
  delay(1000000);
}
