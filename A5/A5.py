# Praktikum Sensortechnik
# Modul 1: Aufgabe 5
# Paul Krüger

# imports
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# create plot
def create_plot(path):
    # load and manipulate data
    data = pd.read_csv(path, skiprows=1)
    time_step = 0.1
    T = np.arange(0, len(data["SI"])*time_step, time_step)

    # create graphic
    fig, ax1 = plt.subplots(figsize=(10, 4))
    ax2 = ax1.twinx()
    ax2.set_ylim((-0.05,1.8))
    ax1.set_ylabel("SI-Wert in $\mathrm{m s^{–1}}$")
    ax2.set_ylabel("PGA-Wert in $\mathrm{m s^{–2}}$")
    ax1.plot(T, data["SI"], 'gx')
    ax2.plot(T, data["PGA"], 'bx')
    ax1.set_xlabel("Zeit in s")
    ax1.yaxis.label.set_color('g')
    ax2.yaxis.label.set_color('b')

    # save plot
    plt.savefig(path[:-3] + "png", dpi=500)

create_plot("/Users/kruegepa/Desktop/Universität/5. Semester/Sensortechnik/Modul 1/Erdbebendetektion/A5/A5.3 Data.txt")