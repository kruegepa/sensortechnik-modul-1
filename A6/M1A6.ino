#include <D7S.h>
#include <Adafruit_MMA8451.h>
#include <Adafruit_Sensor.h>

//old earthquake data
float oldSI = 0;
float oldPGA = 0;

//flag variables to handle collapse/shutoff only one time during an earthquake
bool shutoffHandled = false;
bool collapseHandled = false;

Adafruit_MMA8451 mma = Adafruit_MMA8451();


void setup()
{
  // SETUP EARTHQUAKE
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial)
  {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println("Praktikum Sensortechnik");
  Serial.print("Starting D7S communications (it may take some time)...");
  //start D7S connection
  D7S.begin();
  //wait until the D7S is ready
  while (!D7S.isReady())
  {
    Serial.print(".");
    delay(500);
  }
  Serial.println("STARTED");

  //setting the D7S to switch the axis at inizialization time
  Serial.println("Setting D7S sensor to switch axis at inizialization time.");
  D7S.setAxis(SWITCH_AT_INSTALLATION);

  Serial.println("Initializing the D7S sensor in 2 seconds. Please keep it steady during the initializing process.");
  delay(2000);
  Serial.print("Initializing...");
  //start the initial installation procedure
  D7S.initialize();
  //wait until the D7S is ready (the initializing process is ended)
  while (!D7S.isReady())
  {
    Serial.print(".");
    delay(500);
  }
  Serial.println("INITIALIZED!");


  //reset the events shutoff/collapse memorized into the D7S
  D7S.resetEvents();

  Serial.println("\nListening for earthquakes!");

  // SETUP ACCEL  
  Serial.println("Adafruit MMA8451 test!");
  

  if (! mma.begin()) {
    Serial.println("Couldnt start");
    while (1);
  }
  Serial.println("MMA8451 found!");
  
  mma.setRange(MMA8451_RANGE_2_G);
  
  Serial.print("Range = "); Serial.print(2 << mma.getRange());  
  Serial.println("G");
  
}


void loop() {
  // EARTHQUAKE
  Serial.print(D7S.getInstantaneusSI());
  Serial.print(",");
  Serial.print(D7S.getInstantaneusPGA());
  Serial.print(",");

  // ACCEL
  mma.read();
  Serial.print(mma.x); 
  Serial.print(",");
  Serial.print(mma.y); 
  Serial.print(",");
  Serial.print(mma.z); 
  Serial.print(",");

  sensors_event_t event; 
  mma.getEvent(&event);
  Serial.print(event.acceleration.x);
  Serial.print(",");
  Serial.print(event.acceleration.y);
  Serial.print(",");
  Serial.print(event.acceleration.z);

  Serial.println();
  delay(100);
}

