# Praktikum Sensortechnik
# Modul 1: Tram-Messung
# Paul Krüger

# imports
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.fft import rfft, rfftfreq


# analyze by Fast Fourier Transformation
def analyze_fft(path):
    # load data
    data = pd.read_csv(path, skiprows=1)

    # manipulate data
    data["X"] = data["X"] - np.mean(data["X"][:10000])
    data["Y"] = data["Y"] - np.mean(data["Y"][:10000])
    data["Z"] = data["Z"] - np.mean(data["Z"][:10000])

    # settings
    freqency = 50  # Hz
    N = len(data["X"])
    plt.figure(figsize=(10, 6))

    # x-axis    
    plt.subplot(3,1,1)
    yf = rfft(np.array(data["X"]))
    xf = rfftfreq(N, 1 / freqency)
    plt.xlabel("Frequenz in Hz")
    plt.plot(xf, np.abs(yf), label="x-Achse")
    plt.legend(loc="upper right")

    # y-axis
    plt.subplot(3,1,2)
    yf = rfft(np.array(data["Y"]))
    xf = rfftfreq(N, 1 / freqency)
    plt.xlabel("Frequenz in Hz")
    plt.ylabel("Amplitude")
    plt.plot(xf, np.abs(yf), label="y-Achse", color="darkorange")
    plt.legend(loc="upper right")

    # z-axis
    plt.subplot(3,1,3)
    yf = rfft(np.array(data["Z"]))
    xf = rfftfreq(N, 1 / freqency)
    plt.xlabel("Frequenz in Hz")
    plt.plot(xf, np.abs(yf), label="z-Achse", color="green")
    plt.legend(loc="upper right")
    plt.savefig(path[:-4] + "FFT.png", dpi=500, bbox_inches='tight')


analyze_fft("M1J.txt")
analyze_fft("M2J.txt")
analyze_fft("M3J.txt")
analyze_fft("M4J.txt")
analyze_fft("M6J.txt")
analyze_fft("M7J.txt")
analyze_fft("M8J.txt")
