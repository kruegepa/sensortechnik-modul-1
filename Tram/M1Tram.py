# Praktikum Sensortechnik
# Modul 1: Tram-Messung
# Paul Krüger

# imports
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# generate single plots
def plot_single_measurement(path, start_time=0, step=10):
    """
    Parameters:
        path:       path to data
        start_time: time (in s) from which evaluation should start

    Returns:
        plot of data
    """
    # load data
    data = pd.read_csv(path, skiprows=1)

    # manipulate data
    data["X"] = data["X"] - np.mean(data["X"][:10000])
    data["Y"] = data["Y"] - np.mean(data["Y"][:10000])
    data["Z"] = data["Z"] - np.mean(data["Z"][:10000])
    x_mean = []
    y_mean = []
    z_mean = []

    # create time axis
    if path[-5] == "J":
        time_step = 0.02  # s
        step = 50
    else:
        time_step = 0.5  # s
        step = 2

    T = np.arange(0, len(data["X"])*time_step, 1)
    
    # calculate mean
    for i in range(step, len(data["X"]), step):
        x_mean.append(np.mean(data["X"][i-step:i]))
        y_mean.append(np.mean(data["Y"][i-step:i]))
        z_mean.append(np.mean(data["Z"][i-step:i]))

    # create plot
    plt.figure(figsize=(10, 6))
    start = int(start_time // time_step)

    # x-axis
    plt.subplot(3,1,1)
    plt.plot(T[start:-1] - start_time, x_mean[start:], label="x-Achse")
    plt.xlabel("Zeit in s")
    plt.legend(loc="upper right")
    print(np.max(x_mean[start:]))

    # y-axis
    plt.subplot(3,1,2)
    plt.plot(T[start:-1] - start_time, y_mean[start:], label="y-Achse", color="darkorange")
    plt.xlabel("Zeit in s")
    plt.ylabel("Beschleunigung in $\mathrm{m/s^2}$")
    plt.legend(loc="upper right")
    print(np.max(y_mean[start:]))

    # z-axis
    plt.subplot(3,1,3)
    plt.plot(T[start:-1]  - start_time, z_mean[start:], label="z-Achse", color="green")
    plt.xlabel("Zeit in s")
    plt.legend(loc="upper right")
    print(np.max(z_mean[start:]))

    # save plot
    plt.savefig(path[:-3] + "png", dpi=500, bbox_inches='tight')
    
    
# main
plot_single_measurement("M1J.txt")
plot_single_measurement("M2J.txt")
plot_single_measurement("M3J.txt")
plot_single_measurement("M4J.txt")
plot_single_measurement("M6J.txt")
plot_single_measurement("M7J.txt")
plot_single_measurement("M8J.txt")
# plot_single_measurement("M2P.txt")
# plot_single_measurement("M3P.txt")
# plot_single_measurement("M4P.txt")
# plot_single_measurement("M5P.txt")
# plot_single_measurement("M6P.txt")
